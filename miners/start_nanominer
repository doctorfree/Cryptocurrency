#!/bin/bash

MINER_DIR="/usr/local/nanominer"
MINER_EXE="${MINER_DIR}/nanominer"
MINER_LOG="/var/run/miner.output"
USER=`id -u -n`
GRP=`id -g -n`

[ -x ${MINER_EXE} ] || {
  echo "${MINER_EXE} does not exist or is not executable. Exiting."
  exit 1
}

usage() {
  echo "Usage: start_nanominer [-c coin] [-d delay] [-s] [-u]"
  echo "Where: coin is 'ergo' or 'etc' (default 'etc')"
  exit 1
}

# Exit if miner already running or miner disabled
[ -x /usr/local/bin/check_nanominer ] && {
  /usr/local/bin/check_nanominer -q
  [ $? -gt 0 ] && {
    exit 1
  }
}

[ -f ~/.nominer ] && exit 1

sudo touch ${MINER_LOG}
sudo chown ${USER}:${GRP} ${MINER_LOG}
coin="etc"
delay=0
silent=
while getopts c:d:su flag; do
  case $flag in
    c)
      coin="$OPTARG"
      ;;
    d)
      delay="$OPTARG"
      ;;
    s)
      silent=1
      ;;
    u)
      usage
      ;;
  esac
done
shift $(( OPTIND - 1 ))

while [ $delay -gt 0 ]
do
  delay=`expr $delay - 1`
  [ "$silent" ] || echo -e -n "Starting miner in $delay seconds  \r"
  sleep 1
done

cd ${MINER_DIR}

if [ -f config_${coin}.ini ]
then
  ${MINER_EXE} config_${coin}.ini > ${MINER_LOG} 2>&1
else
  echo "${MINER_DIR}/config_${coin}.ini does not exist. Exiting."
  exit 1
fi
