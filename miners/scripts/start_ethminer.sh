#!/bin/bash

EWAL="0x765972c4E777096CF94AEE2432a81B9510DfE6e4"
EPOOL="us2.ethermine.org:5555"
FPOOL="us1.ethermine.org:5555"
LOG="/var/run/miner.output"
PORT=8080
USER=`id -u -n`
GRP=`id -g -n`
WORKER=${USER}
#
# Use sequential DAG load if miner crashes during parallel load
# 0=parallel, 1=sequential
MODE="1"
#
# Set temperature cutoff and restart limits
# 0 is disabled, valid: 30..100
#
# Stop mining on a GPU if temperature exceeds value.
# --tstop UINT=90
#
# Restart mining on a GPU if the temperature drops below.
# --tstart UINT=70
TSTOP=90
TSTART=70

sudo touch $LOG
sudo chown ${USER}:${GRP} $LOG
sudo /usr/local/bin/ethminer \
                        --dag-load-mode ${MODE} \
                        --HWMON 1 \
                        --opencl \
                        -P stratum+ssl://${EWAL}.${WORKER}@${EPOOL} \
                        -P stratum+ssl://${EWAL}.${WORKER}@${FPOOL} \
                        --farm-recheck 2000 \
                        --report-hashrate \
                        2>&1 | /usr/bin/tee -a /var/run/miner.output
