#!/bin/bash

TOP="$HOME/src/Cryptocurrency/miners"

[ -d "$TOP" ] || {
    echo "$TOP does not exist or is not a directory. Exiting."
    exit 1
}

cd "$TOP"
[ -d Autolykos2_AMD_Miner ] || {
    [ -x ./clone-amdminer.sh ] || {
        echo "$TOP/clone-amdminer.sh does not exist or is not executable. Exiting."
        exit 2
    }
    ./clone-amdminer.sh
}
cd Autolykos2_AMD_Miner/Ubuntu
make clean
make
[ "$1" == "-i" ] && {
  AMDMINER_DIR="/usr/local/amdminer"
  instfiles="../config.json ErgoOpenCL makefile MiningKernel.cl objects.mk \
             OCLdecs.h OCLdefs.h PreHashKernel.cl sources.mk subdir.mk"
  [ -d ${AMDMINER_DIR} ] || sudo mkdir -p ${AMDMINER_DIR}
  for ifile in ${instfiles}
  do
    [ -f ${ifile} ] || {
      echo "Missing amdminer installation file: ${ifile}"
      continue
    }
    sudo cp ${ifile} ${AMDMINER_DIR}
  done
  sudo chmod 755 ${AMDMINER_DIR}/ErgoOpenCL
}
