#!/bin/bash

project="lotus"

[ -d ${project} ] || {
    ./clone-lotus.sh
}

cd ${project}

git checkout master # mainnet
#git checkout ntwk-calibration # calibration-net
#git checkout ntwk-nerpa # nerpa-net
