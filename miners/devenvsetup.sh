#!/bin/bash

#sudo apt-get -y install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
#sudo add-apt-repository -y ppa:ethereum/ethereum-qt
sudo apt-get update
sudo apt-get install git cmake libtool libcryptopp-dev libleveldb-dev libjsoncpp-dev libboost-all-dev libgmp-dev libreadline-dev libcurl4-gnutls-dev ocl-icd-libopencl1 opencl-headers mesa-common-dev ibmicrohttpd-dev build-essential libjsonrpccpp-dev libjsonrpccpp-stub0 libjsonrpccpp-tools libminiupnpc-dev libminiupnpc10 -y
