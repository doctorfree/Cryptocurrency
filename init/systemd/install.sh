#!/bin/bash

INSTDIR=/lib/systemd/miner
SRCDIR=$HOME/src/init/systemd
STARTE=$HOME/.config/autostart/startminer.desktop
BAK=$HOME/Bak

[ -d $SRCDIR ] || {
    echo "$SRCDIR does not exist or is not a directory. Exiting."
    exit 1
}
cd $SRCDIR
[ -d $INSTDIR ] || sudo mkdir $INSTDIR
for i in *.service
do
    sudo cp $i $INSTDIR
    cd /etc/systemd/system
    sudo ln -s $INSTDIR/$i .
    cd multi-user.target.wants
    sudo ln -s $INSTDIR/$i .
    cd $SRCDIR
done
[ -d $BAK ] || mkdir $BAK
[ -f $STARTE ] && mv $STARTE $BAK
