#!/bin/bash

INSTDIR="/etc/systemd/system"
LIBDIR="/lib/systemd/miner"
#MIN="atisetup checkopencl datagather set-permissions overclock"
MIN="atisetup checkopencl datagather set-permissions"

for i in ${MIN}
do
    sudo rm -f $INSTDIR/$i.service
    sudo rm -f $INSTDIR/multi-user.target.wants/$i.service
    sudo rm -f $LIBDIR/$i.service
done
[ -d $LIBDIR ] && sudo rmdir $LIBDIR
