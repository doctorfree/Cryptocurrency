#!/bin/bash

INSTDIR=/lib/systemd/miner
SRCDIR=$HOME/src/init/systemd
#MIN="atisetup checkopencl datagather set-permissions overclock"
MIN="atisetup checkopencl datagather set-permissions"

[ -d $SRCDIR ] || {
    echo "$SRCDIR does not exist or is not a directory. Exiting."
    exit 1
}
cd $SRCDIR
[ -d $INSTDIR ] || sudo mkdir $INSTDIR
for i in ${MIN}
do
    sudo cp $i.service $INSTDIR
    cd /etc/systemd/system
    sudo ln -s $INSTDIR/$i.service .
    cd multi-user.target.wants
    sudo ln -s $INSTDIR/$i.service .
    cd $SRCDIR
done
