#!/bin/bash

INSTDIR="/etc/systemd/system"
LIBDIR="/lib/systemd/miner"
STARTE="startminer.desktop"
AUTO=$HOME/.config/autostart

for i in *.service
do
    sudo rm -f $INSTDIR/$i
    sudo rm -f $INSTDIR/multi-user.target.wants/$i
    sudo rm -f $LIBDIR/$i
done
[ -d $LIBDIR ] && sudo rmdir $LIBDIR
[ -d $AUTO ] || mkdir -p $AUTO
[ -f $STARTE ] && cp $STARTE $AUTO
