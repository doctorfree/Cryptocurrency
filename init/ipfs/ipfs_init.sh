#!/bin/bash

ETC_IPFS="/etc/ipfs"

[ -d ${ETC_IPFS} ] || sudo mkdir ${ETC_IPFS}
export IPFS_PATH=${ETC_IPFS}

# run ipfs init or move the .ipfs folder if you already did
if [ -d ~/.ipfs ]
then
    sudo mv ~/.ipfs/* /etc/ipfs
else
    sudo ipfs init
fi
# Set permissions - ipfs system user and ipfs group
sudo chown -R ipfs:adm /etc/ipfs
sudo chmod 775 /etc/ipfs

# Files and directories need to be writeable by group adm
