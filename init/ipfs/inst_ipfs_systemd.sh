#!/bin/bash

INST_DIR="/lib/systemd/system"
SERVICE="ipfs.service"

sudo cp $HOME/src/init/systemd/${SERVICE} ${INST_DIR}/${SERVICE}
cd /etc/systemd/system
sudo ln -s $INST_DIR/$SERVICE $SERVICE
