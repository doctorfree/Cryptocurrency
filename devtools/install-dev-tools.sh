#!/bin/bash

sudo apt-get install libssl-dev build-essential automake pkg-config libtool \
                     libffi-dev libgmp-dev libyaml-cpp-dev python-setuptools \
                     python-dev python3-dev autotools-dev libboost-all-dev \
                     autoconf libevent-dev bsdmainutils npm gfortran f2c \
                     r-base r-base-dev ruby-dev

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g express
sudo npm install -g npx
sudo npm install -g truffle
sudo npm install -g ethereumjs-testrpc

# Solc Solidity compiler
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc
