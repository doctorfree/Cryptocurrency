#!/bin/bash

PT_DIR=/usr/local/lib/PtTracker
LOG=PtTracker.log

[ -d ${PT_DIR} ] || {
    echo "${PT_DIR} does not exist or is not a directory. Exiting."
    exit 1
}

cd ${PT_DIR}
pm2 start ${PT_DIR}/pm2-PtTracker.json
sleep 10
inst=`type -p grcat`
if [ "$inst" ]
then
    tail -f ${PT_DIR}/${LOG} | grcat conf.profittrailer
else
    tail -f ${PT_DIR}/${LOG}
fi
