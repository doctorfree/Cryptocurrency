#!/bin/bash

PT_DIR=/usr/local/lib/PtTracker
LOG=PtTracker.log

[ -d ${PT_DIR} ] || {
    echo "${PT_DIR} does not exist or is not a directory. Exiting."
    exit 1
}

tailit() {
    inst=`type -p grcat`
    if [ "$inst" ]
    then
        tail -f ${PT_DIR}/${LOG} | grcat conf.profittrailer
    else
        tail -f ${PT_DIR}/${LOG}
    fi
}

cd ${PT_DIR}

case "$1" in
    help|usage)
        echo ""
        echo "Usage:"
        echo "   ptt [status|start|stop|restart|delete]"
        echo "or"
        echo "   ptt [show|monit|tail]"
        echo ""
        exit 0
        ;;
    start)
        /usr/local/bin/ptt_start
        ;;
    status|stop|restart|delete)
        pm2 $1 PTTracker
        ;;
    show|monit)
        pm2 $1 PTTracker
        ;;
    tail)
        tailit
        ;;
    *)
        ptt help
        exit 1
        ;;
esac
exit 0

